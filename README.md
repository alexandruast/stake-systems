# Stake Systems Infrastructure

Here, at Stake Systems, we are building infrastructure to support awesome projects running in the blockchain landscape.  
To keep things simple to manage, we develop our own code to build, deploy, monitor, failover and move nodes around with minimal downtime.  

We are hosting loads on multiple providers - Hetzner and Contabo.  
We are using a mix of VPS and Bare Metal, running minimal Ubuntu 18.04 LTS with the latest CIS patching  
CIS Ubuntu Linux 18.04 LTS Benchmark - <https://nvd.nist.gov/ncp/checklist/860>  
Everything we develop in house complies with OWASP and <https://12factor.net>  
OWASP Security Guidelines - <https://owasp.org/www-project-top-ten/>  
Pulling secrets is not something we agree with. Instead, keys are pushed when needed, directly from Hashicorp Vault.  
The only way in into our servers is through SSH, and nothing more.  
Monitoring our infrastructure is done 24x7 and the alerts are delivered, based on severity, via Telegram and SMS/Phone.  
On-call SREs are trained and available 24x7 to respond to any incident.  
Our SRE/DevOps are also stakeholders, so the responsibility is paramount.  

Because we are complexity bigots, we detest clutter and we refuse to deliver bells and whistles.  
We deliver minimalism and simplicity, instead.  

We strongly believe in the following quote:  
"Perfection is achieved, not when there is nothing more to add, but when there is nothing left to take away."  
~ Antoine de Saint-Exupery

## Tendermint

* cosmos, iris, kava, e-money

We are currently supporting Tendermint-based chains, with the following perks:

* DoS/DDoS protection
* hot standby with two nodes, on different providers
* logical sentry layer - one instance, multiple external IPs, sentry containers exposed, validator instance/container isolated
* physical sentry layer - multiple instances each with its own external IP, validator instance/container isolated

## Solana

* TdS and Mainnet

We are currently supporting Solana with the following perks:

* DoS/DDoS protection
* warm standby node
